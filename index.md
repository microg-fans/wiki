---
layout: page
title: microG
permalink: /index
---


[](#microg)

## Introduction

This wiki is intended to serve as an unofficial guide to set up an Android phone using microG in order to avoid privacy invasive Google Apps (GApps) which come pre-installed on most Android phones. Portions of this wiki are taken from the NoGoolag Telegram Group, [Nizo's guide for the installation process](https://github.com/theNizo/installMicroG/), [Nanodroid](https://gitlab.com/Nanolx/NanoDroid) and official [microG documentation](https://microg.org/). Thank you to all of the original authors.

## Disclaimer

> **Neither this wiki or contributor to it is responsible for bricked devices, dead SD cards, thermonuclear war, or you getting fired because the alarm app failed. If you follow instructions carefully this should not happen, but every change or modification you make is your choice and your responsibility. That being said if you think you have any issues please ask for help.**

Also note that your warranty may be void if you tamper with any part of your device / software. Refer to your phone's manufacturer for any more information on this.


## Why microG?

microG replaces GApps providing you with:

- Privacy
- No Google
- Better performance
- Longer battery life
- More open source

## Do you want microG?

microG is for people who want to run their phone without Google Services but also want to continue to use apps which need Google Play Services (which are only Google Apps for the most part). If you're looking for maximum security and privacy on your phone, you want [GrapheneOS](https://grapheneos.org/). microG is for if you want both privacy and continued app support, ridding your phone of Google Apps while letting apps that deped on it continue to work. Not everything works on microG, so be prepared.

## Is microG a security risk?

### Signature Spoofing

microG requires signature spoofing permissions to function. If granted to a malicious app, signature spoofing permissions could pose a serious security risk. It is therefore advised that you do not grant this permission to any app other then microG if it is available. 

*(The following statement is possibly outdated (see [NanoDroid Issue Tracker](https://gitlab.com/Nanolx/NanoDroid/-/issues/128#note_398056126) )*
It's recommended to use a ROM that supports Signature Spoofing as NanoDroid automatically grants it to every app trying to spoof signature. If you use LineageOS for microG there is no added sceurity risk as the signature spoofing permissions are not available to any other app.

### Custom ROM

Unlocking your phones bootloader and using a custom ROM can be a security risk regardless of microG. Before taking steps to unlock your bootloader the user should consider the risks and take appropiate precautions to protect yourself.

## What is microG?

microG is an open source and private implementation of the proprietary GApps. microG has 7 main components. microG is able to work without the components that are not marked as 'needed', however if you want the features, install them:

- (needed) **GmsCore** (com.google.android.gms)

most of microG is within GmsCore, the replacement for Google Play Services.

- **GsfProxy** (com.google.android.gsf)

enables Google Cloud Messaging (GCM) which allows notifications to work from most apps.

- (needed) **FakeStore** (com.android.vending)

tiny app that pretends to be Google Play to convince apps that it is present on your device. This is not a replacement for Google Play, for that see [Aurora Store](wiki/aurorastore).

- **MapsAPIv1** (com.google.android.maps)

enabled apps that request maps to use OpenStreetMap (provided by Mapbox)

- **UnifiedNLP**

Android has a built-in location backend that some apps (like [OSMand](https://f-droid.org/en/packages/net.osmand.plus/)) use, but most apps use the GApps location backend (because Google recommends it). This enables the GApps location backend implementation to work without GApps. Requires a [location backend](wiki/location) (LineageOS for microG and other installers come with backends). Can be installed on its own without microG with [the min microG installer UNLP edition](wiki/minmicrog#wiki_unifiednlp_edition).



## Getting microG

### Prerequisites 

- **Ability to unlock the bootloader** - To properly use microG, you need a [custom ROM](https://www.xda-developers.com/what-is-custom-rom-android/) (alternative firmware on your phone). Installation of custom ROMs requires unlocking the bootloader. Instructions to unlock your bootloader differ per device. Instructions to do this can be normally found on [XDA-Developers forum](https://www.xda-developers.com/), from a web search, Youtube tutorials or a subreddit specific to your device. Some devices, notably newer Huawei devices and Nokia devices, do not allow unlocking the bootloader.

- **Custom recovery** – Because it *is* required to have a custom ROM with Signature Spoofing support in order to take advantage of microG, it is also required to have a custom recovery in order to flash it. The most popular custom recovery is [TWRP](https://twrp.me/) as it is supported by many phones and is objectively considered as the best custom recovery. For devices that officially support TWRP, it is easily searched in [TWRP's website](https://twrp.me/Devices/). For those that have a smartphone that does not officially support TWRP, it is possible to find a custom recovery in [XDA's website](https://www.xda-developers.com/search2/) by searching for your phone.

- **Custom ROM with Signature Spoofing support** – microG requires a process called Signature Spoofing in order to pretend to be Google Play Services so it can function, which no stock ROM supports this particular feature. Be aware: a custom ROM may also **not** support Signature Spoofing (this includes LineageOS, [LineageOS for microG](wiki/lineage4microg) exists to use microG with Lineage). Some custom ROMs that support Signature Spoofing are listed in [this wiki page](wiki/supportedroms). It is possible to use [a patcher to patch a ROM and add signature spoofing support](wiki/patching), but this is only recommended for advanced users. The most popular ROM to use with microG is LineageOS for microG, which comes with microG set up out of the box and does not require any further installation. If a ROM doesn't have Signature Spoofing support, it has to be patched, eg. with NanoDroid (links listed below).

- **Patience!** – Patience is heavily required, especially if it is your first time doing this because it will be a new experience for you. Don't be afraid to ask for help if you get stuck.

## How to install it?

### Choose your method:

There are multiple known methods to install and take advantage of microG. Both methods A and B are very simple and should be able to be done by most people with reasonable technical knowledge. C is more complicated, but will work if you follow the guide below.

#### [A. LineageOS for microG](wiki/lineage4microg), [OmniROM with microG](wiki/omni), or [CalyxOS](wiki/calyxos) (easiest)

The easiest way to get microG is with [OmniROM for microG](wiki/omni) or [LineageOS for microG](wiki/lineage4microg). Both come with microG pre-installed so there is no need to flash an installer. OmniROM for microG builds are officially published by the OmniROM team, LineageOS for microG is an unofficial fork of LineageOS. CalyxOSS is an option for Pixel phones and the Xiaomi A2 that comes with a bundled microG and supports relocking of the bootloader for extra security.


#### [B. a microG Installer](wiki/installers)

If you want to use microG with a [ROM that has Signature Spoofing support](wiki/supportedroms), the easiest way to do this is with an installer. To use an installer you need to simply flash one in place of a GApps package. To find an installer which is right for you, head over to [the installers wiki page](wiki/installers).

#### C. Manual installation (hardest)

In the manual way, you install the APKs yourself. Go to the [microG's downloads page](https://microg.org/download.html), or use the F-Droid [repo](https://microg.org/fdroid.html). 'Services Cores' and 'FakeStore' are the essential apps you need to get a basic setup running. Also install other apps, if you need the services they provide.

### Install microG

0. Back up data on your phone to an external location, as wiping the phone will erase all your data.
1. Wipe your phone from your custom recovery of choice (probably TWRP).
2. Flash your ROM of choice.
3. If you use an installer, flash it now, according to its instructions. If you do manual installation and your ROM doesn't support Signature Spoofing, flash a patch like [Nanodroid](wiki/nanodroid) (NanoDroid takes 10-15 minutes to install, be patient).
4. Boot
5. For manual installation, install apps; [see here](index#c-manual-installation-hardest)
6. [Optional] Go to Settings > Accounts > Add Accounts > Google and login to your account. Required if your installer includes Google Play and you want to use it.
7. Open 'microG Settings' > Self-Check. If "System grants signature spoofing permission" isn't ticked, click on it and grant permission. DO NOT DISABLE BATTERY OPTIMIZATION YET.
8. [Optional, but recommended] In microG settings, Go to "Google device registration", "Google Cloud Messaging" and enable both of them.
9. Reboot.
10. (If you don't want the location backend, skip to step 12) In microG settings, go to UnifiedNLP settings, and enable everything in "Configure location backends" and "Configure address lookup backends".
11. Reboot.
12. Open 'microG Settings' > Self-Check and disable battery optimization (tick the checkmark and accept).
13. Check the Self Check in microG Settings again, if anything is not ticked check the **[common issues wiki page](wiki/issues)**.
14. If you have any issues, [make a post on lemmy](https://discuss.tchncs.de/c/microg) to ask for help. 

Once installed, have a look at the rest of this wiki for info and tips on using microG.

## Using microG

### [Common issues with microG](wiki/issues)

### Alternatives and instructions to use Google Apps with microG

#### [Google Contacts / Calendar Sync](wiki/contacts)
#### [Google Maps](wiki/maps)
#### [Chrome](wiki/browser)
#### [Google Photos](wiki/photos)
#### [Google Play](wiki/store)
#### [Youtube](wiki/youtube)
#### [Google Messages / Hangouts](wiki/messaging)
#### [Gmail](wiki/mail)
#### [Google Search](wiki/search)
#### [GBoard](wiki/keyboard)
#### [Google Drive (Cloud / Sync)](wiki/cloud)
#### [Google Keep](wiki/notes)
#### [Google Duo](wiki/videochat)
#### [Google Play Games](wiki/playgames)
#### [Google Voice](wiki/voice)
#### [Google / Chrome Password Manager](wiki/passwords)
#### [Google Translate](wiki/translate)
#### [Google Assistant](wiki/assistant)
#### [Google Text-to-Speech Engine](wiki/tts)
#### [Google Device Backup](wiki/backup)

### Other alternatives

#### [Reddit](wiki/reddit0)
#### [Facebook](wiki/facebook)
#### [Twitter](wiki/twitter)

NOTE: more alternatives can be found at the bottom of the page; under *Free and Open Source Android Apps*

### [Other Android Privacy Tools](wiki/privacytools)
### [Location Backends](wiki/location)
### [Aurora Store information](wiki/aurorastore)
### [Patched Play Store Guide](wiki/patchedplay)

### [Smart Watch / WearOS](wiki/smartwatch)

### Apps that do not work

#### [List of apps that are known not to work](wiki/notworking)

#### [Instructions to get apps to work that do not normally work](wiki/getworking)

#### [What to do if an app doesn't work](wiki/troubleshooting)

### Get Support

#### [Make a post on /r/microG](/r/microG/submit?selftext=true)

- Ask about anything, help, bug reports and anything in between.
- Please make sure to include all relevant information, what ROM you are using, what app version, instructions to reproduce etc/
- Do *not* ask for support about LineageOS for microG in the official LineageOS subreddit. They do not support microG (and sometimes like to pretend it doesn't exist). Ask for support in /r/microG.

#### [Open a bug on GitHub](https://github.com/microg/android_packages_apps_GmsCore/issues)

- Make sure you are reporting the bug in the right place (**[check this flow chart first](https://lineage.microg.org/images/bug_reporting.svg)**).
- Make sure it is a bug reproducible by anyway, if in doubt post here (on /r/microG) not on GitHub.
- Make sure the bug has not already been reported (search through previous bug).

#### [Official microG IRC channel](irc://#microg@irc.libera.chat)

You can join #microg on Libera Chat. It can also be joined using [Libera's webchat client](https://web.libera.chat/?channel=#microg), or via the bridged Matrix room [#microG:matrix.org](https://matrix.to/#/#microG:matrix.org) (note the capital).


## Official links

### microG

- [Project website](https://microg.org/)
- [Official wiki on GitHub](https://github.com/microg/android_packages_apps_GmsCore/wiki)
- [Issues on Github](https://github.com/microg/android_packages_apps_GmsCore/issues)

### NanoDroid

- [Nanodroid GitLab](https://gitlab.com/Nanolx/NanoDroid)
- [Nanodroid on XDA](https://forum.xda-developers.com/apps/magisk/module-nanomod-5-0-20170405-microg-t3584928)

## External links

#### Free and Open Source Android Apps

- [Ashpex / Android FOSS Apps](https://gitlab.com/Ashpex/android-FOSS-apps)
- [Droidbreak (privacy replacements for common Android apps)](https://droid-break.info/)
- [Mybridge / amazing-android-apps](https://github.com/Mybridge/amazing-android-apps)
- [pcqpcq / open-source-android-apps](https://github.com/pcqpcq/open-source-android-apps)
- [TheEvilSkeleton / free-and-open-source-android-apps](https://github.com/TheEvilSkeleton/free-and-open-source-android-apps)
- [unicodedeveloper / awesome-opensource-apps](https://github.com/unicodeveloper/awesome-opensource-apps#android)
- [Primokorn / FLOSS Android Apps](https://gitlab.com/Primokorn/FLOSS_Android_apps/)