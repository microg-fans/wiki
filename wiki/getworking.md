---
layout: page
title:  "Get apps to work"
tags: issue
---

## General cases

### Paid apps, license verification and IAPs

You can directly buy apps from the Play Store if you have it installed with microG. But if you don't, a much cleaner way is to buy it from the [Google Play](https://play.google.com) website through a browser.

If you've bought an app, you can download it without Play Store by using Aurora Store by logging in with your own account.

License verification, unfortunately, is something tied to Play Store and probably always will be. If you don't want to install it, all you can do is pester the devs to remove it or atleast offer alternative means of verification.

In App Purchases are even more tied to Play Store. Not even vanilla Play Store will do, it has to be patched for sigspoofing to be able to use IAPs. Setialpha regularly grabs the latest release and patches it, and the product can be found in the NanoDroid F-Droid repository and in various packs.

## Specific apps

### Google Maps

Sign out of Google Maps (in settings) and then it will work.

### Titanium Backup

The Titanium Backup devs are a bunch of nice people. If you email them, they'll give you offline verification codes.

### Malaysian online banking apps / GrabPay (within Grab)

Due to national bank regulations on phone modification, most if not all online banking apps from Malaysian banks will refuse to work with microG, instead complaining about lack of Google Play Services.

To fix this, you **need** to root your phone with Magisk and use MagiskHide on them in order for the apps to work.

### Grab

#### UI not loading after launch

Grab will not load the UI at launch without you explicitly enabling location *before* launching.

If you already opened the app, close the app first, then enable location before relaunching it.

#### Unable to log into your account with your phone number

If you already previously logged into Grab on another device, log off from there first.

There *is* still a chance that your current device's Grab won't allow you to log back into your original account with unknown error. In this case, contact Grab's customer support for them to temporarily reset your account then log in as soon as possible within the period that the customer support tells you.