---
layout: page
title:  "Chrome on microG"
tags: alternative
---

## Ideal Open Source Alternatives

### Firefox (Fennec)

- Avalible on Google Play (download through [Aurora Store](wiki/store))
- [Unofficial F-droid repo](https://gitlab.com/rfc2822/fdroid-firefox) *Official builds of Firefox are added to this repo daily*

*note that firefox is avalible on f-droid under the name Fennec, but it receives vital security fixes slower then regular Firefox, so I would reccomend against it*

Firefox, secure, google-free browser with built in enhanced tracking protection and add-on support.

### Firefox Nightly (Fenix, ultimately the successor to Fennec)

- Avalible on Google Play (download through [Aurora Store](wiki/store))
- [Unofficial F-droid repo](https://gitlab.com/rfc2822/fdroid-firefox) *Official builds are added to this repo daily*

Much faster and stable variant of Firefox to replace Firefox when it has achieved feature maturity. Limited add-on support yet, but still features enhanced tracking protection.

## Use Chrome with microG

### Bromite

- Avalible from [Bromite.org](https://www.bromite.org/) and [Bromite's F-droid repo](https://www.bromite.org/fdroid)

Bromite is a more secure and google-free fork of Chrome.

Bromite also provides a system web-view (built-in system-wide viewer for embedded web pages). You should not install this unless you know what you are doing, as it conflicts with and overrides the built-in Android web view, and might make some apps unable to render HTML content.

### Chrome

Chrome should work if downloaded from the [Aurora Store](wiki/store). However, Chrome installs a system webview that overrides the built in Android system webview. This may not be what you want.