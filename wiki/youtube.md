---
layout: page
title:  "Youtube on microG"
tags: alternative
---

## Ideal Open Source Alternatives

### Newpipe

**[Avalible on F-droid](https://f-droid.org/en/packages/org.schabi.newpipe/)** (for Android 4.xx, see [Newpipe Legacy](https://f-droid.org/app/org.schabi.newpipelegacy))

Open-source Youtube client.

Advantages over Youtube:

- Free, open source and private
- Background and pop-out player
- Downloads
- Support for Peertube, Soundcloud and more.

Disadvantages over Youtube:

- No suggestions
- Wierd subscription sorting, to be fixed soon (PR currently open on Github).
- switching from the special player to main app is not implemented

### Skytube

**[Avalible on F-droid](https://f-droid.org/packages/free.rm.skytube.oss/)** (Skytube) or **[Avalible on Skytube's Website](https://skytube-app.com/)** (Skytube Extra)

Open Source Android client. Two different versions exist, Skytube (completley open source) and Skytube Extra (partly not open source).

Feature 	|SkyTube Extra 	|SkyTube
------|-----|----
Description 	|Contains extra features that are powered by non-OSS libraries. 	|Fully open-source and free software.
Licence 	|GPLv3 	|GPLv3
Official YouTube player support| 	yes|no
Updates publishing delays 	|Immediate 	|Normally up to 5 days


## Use Youtube with microG

### App

It is possible to use Youtube with microG through Youtube Vanced. Download the non-root Youtube Vanced from [the official website](https://vanced.app/) to use it with microG. Includes sign in and suggestions, no ads, background and pop-up play.

### Mobile Web Site / GApps Browser

The Youtube mobile website works fine through microG in any browser, or can be accessed through the [GApps Browser app](https://f-droid.org/en/packages/com.tobykurien.google_news/).