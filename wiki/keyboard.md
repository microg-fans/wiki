---
layout: page
title:  "GBoard on microG"
tags: alternative
---

## Ideal Open Source Alternatives

### AnySoftKeyboard

- **[Available on F-droid](https://f-droid.org/en/packages/com.menny.android.anysoftkeyboard/)**
- **[Available on Google Play](https://play.google.com/store/apps/details?id=com.menny.android.anysoftkeyboard)**

Offline but feature rich open source keyboard. Includes themes, emoji, gestures, autocomplete, customization and swipe typing. Note that the swipe typing is in beta and currently quite unusable. The version from Google Play is updated more frequently, and is newer than the F-Droid version. The swipe typing is much improved in the newer version.

Advantages over GBoard:

- Free, open source and private
- Can run without Google Play Services

Disadvantages over GBoard:

- Swipe typing doesn't work very well
- Autocorrect isn't as good


### AOSP Keyboard / Openboard


**Built into most ROMs as the default keyboard and [available on F-droid with a few tweaks named OpenBoard](https://f-droid.org/en/packages/org.dslul.openboard.inputmethod.latin/)**

Simple standard and default keyboard from the Android Open Source Project. It is possible to get good and reliable swipe typing working by taking some libraries from a GApps package and manually adding it using ADB through a complex process. [Nanodroid](wiki/Nanodroid) or [minmicroG](wiki/minmicroG) standard edition [installers](wiki/installers) includes this libraries and will install them. If this doesn't work for you, try EnhancedIME:

### EnhancedIME

**[Available on XDA](https://forum.xda-developers.com/android/apps-games/enhancedime-aosp-latinime-enhancements-t3366639)**

AOSP keyboard with modifications. Numbers row at the top, swipe support (with libraries, see above) and more themes. Installs on top of AOSP. If regular install fails try installing through root with MiXplorer.



## Use GBoard on microG

Gboard works perfectly well with microG.