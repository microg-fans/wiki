---
layout: page
title:  "Google Keep on microG"
tags: alternative
---

## Ideal Open Source Alternatives

### Joplin (Markdown-based note and to-do)
**[Available on GitHub](https://github.com/laurent22/joplin-android/releases)** | **[Homepage](https://joplinapp.org/)** 

The notes can be either stored locally on the device and synced with a third-party like SyncThing-fork (see [Cloud / Sync](wiki/cloud)), or synced with cloud-based services like NextCloud, WebDAV, Dropbox and OneDrive. The notes can also be End-to-End Encrypted using a passphrase. A desktop client is also available.

### Standard Notes
**[Available on F-droid](https://f-droid.org/en/packages/com.standardnotes/)** | **[Homepage](https://standardnotes.org/)**

A simple, cross platform, open source and encrypted notes app. Basic note creation, editing and sync is free. Costs for more advanced features like 2FA, themes, backup etc.

### Markor
**[Available on F-droid](https://f-droid.org/en/packages/net.gsantner.markor/)**

A very lightweight, open source and simple offline markdown notes app.

## Using Google Keep with microG
### App

~~I am not sure if it is possible to use the Google Keep app with microG, please update if you know.~~
Edit: Google Keep app works perfectly with MicroG

### Mobile Web Site / GApps Browser

The Google Keep mobile website works fine through microG in any browser, or can be accessed through the [GApps Browser app](https://f-droid.org/en/packages/com.tobykurien.google_news/).