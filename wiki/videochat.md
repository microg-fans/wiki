---
layout: page
title:  "Google Duo on microG"
tags: alternative
---

## Ideal Open Source Alternatives

### Signal

**Available on the Google Play Store (download through [Aurora Store](wiki/store))**

**[Official auto-updating APK](https://signal.org/android/apk/)** *for people who cannot install the app through the Play Store*

[Unofficial F-droid repo](https://gitlab.com/rfc2822/fdroid-firefox) *unofficial builds of Signal are added to this repo daily*

End-to-end encrypted and open source instant messaging program with support for audio and video calling.

Advantages over Google Duo:

- Free, open source and private
- End-to-end encrypted
- Can run without Google Play Services

Disadvantages over Google Duo:

- Not as many users

**Use without Play Services:** If you want to use Signal without Play Services (or not have it register to Google Cloud Messaging through MicroG), make sure that you turn off Google Cloud Messaging in microG settings (or require apps to ask before registration) before you install the Signal APK. Signal will automatically detect if it is on a device that does not have Play Services, and instead use a persistent background connection. Note that this can drain your battery somewhat faster.


### Wire

**Available on the Google Play Store (download through [Aurora Store](wiki/store))** [Official Webitse](https://wire.com/en/)

>Wire offers the most comprehensive collaboration suite featuring messenger, voice, video, conference calls, file-sharing, and external collaboration – all protected by the most secure end-to-end-encryption.

### Jitsi
**[Avalible on F-droid](https://f-droid.org/en/packages/org.jitsi.meet/)** | [Official Site](https://jitsi.org)

>
Multi-platform
open-source video conferencing

>At Jitsi, we believe every video chat should look and sound amazing, between two people or 200. Whether you want to build your own massively multi-user video conference client, or use ours, all our tools are 100% free, open source, and WebRTC compatible.


### Linphone
**[Avalible on F-droid](https://f-droid.org/en/packages/org.linphone/)** | [Official Website](https://www.linphone.org)

FOSS VOIP multi-platform software. Offers "high definition audio and video calls".

### Nextcloud Talk
**[Avalible on F-droid](https://f-droid.org/en/packages/com.nextcloud.talk2/)** | [Official Website](https://nextcloud.com/talk)

Secure team collaboration platform with video calls / conferencing support. Much like Slack. Self hosted on Nextcloud (see [the Google Drive page](wiki/cloud#wiki_nextcloud_.28self-hosted.29))

### Jami
**[Avalible on F-droid](https://f-droid.org/en/packages/cx.ring)** | [Official Website](https://jami.net)

Distributed and open source messaging, video calling and collaboration suite.

### Matrix
[Official website](https://matrix.org)

Secure decentralised communication platform for messaging and VOIP.



### Use Google Duo with microG

Unknown if this is possible, please update if you do.