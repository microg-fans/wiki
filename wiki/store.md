---
layout: page
title:  "App Store on microG"
tags: alternative
---

## Ideal Open Source Alternatives

### F-droid

**[Avalible from F-droid.org](https://f-droid.org/)**

Free and open-source store for free and open-source apps. Built in to LineageOS for microG. All apps on F-droid are open source and privacy respecting (unless listed as an anti-feature).

#### Alternative UIs:

- [Aurora Droid](https://f-droid.org/en/packages/com.aurora.adroid/) 
- [G-Droid](https://f-droid.org/en/packages/org.gdroid.gdroid/)
- [Foxy Droid](https://f-droid.org/en/packages/nya.kitsunyan.foxydroid/)

#### Related subreddits

- /r/fdroid/
- /r/fossdroid/

#### Interesting repos

- [microG F-droid repo](https://microg.org/fdroid/repo?fingerprint=9BD06727E62796C0130EB6DAB39B73157451582CBD138E86C468ACC395D14165): Official repo for microG components.
- [Fdroid-firefox](https://gitlab.com/rfc2822/fdroid-firefox): Non-free official Firefox and Signal builds
- [IzzyOnDroid F-Droid Repository](https://apt.izzysoft.de/fdroid/): A lot of foss apps which are not available in the official repo mostly for technical reasons
- [Nanodroid companion repo](https://nanolx.org/fdroid/repo?fingerprint=862ED9F13A3981432BF86FE93D14596B381D75BE83A1D616E2D44A12654AD015): A repo for Nanodroid users including updates for the Nanodroid patched play store and mpv
- [Bromite F-Droid repository](https://fdroid.bromite.org/fdroid/repo?fingerprint=E1EE5CD076D7B0DC84CB2B45FB78B86DF2EB39A3B6C56BA3DC292A5E0C3B9504): Bromite and Chromium browsers
- [Bitwarden F-Droid repository](https://mobileapp.bitwarden.com/fdroid/): Bitwarden password manager
- [NewPipe F-Droid repository](https://newpipe.net/FAQ/tutorials/install-add-fdroid-repo/): NewPipe Youtube client official builds
- [Known Repositories on F-Droid wiki](https://f-droid.org/wiki/page/Known_Repositories): A bit outdated list


## Use Google Play with microG

### Aurora Store

**[Visit the wiki page on Aurora Store for more information](wiki/aurorastore)**

**[Available on F-droid](https://f-droid.org/en/packages/com.aurora.store/)**

Aurora Store allows to download and install any app from the Play Store anonymously, on microG and without any tracking. You can also log in to your personal account to download/update paid apps or apps in private beta, etc. Note that using the Aurora Store with your personal account technically violates Google's terms of service, and they could ban you. However, there are no reports of this ever happening.

[Aurora Store Telegram Support Group for questions and support](https://t.me/AuroraSupport)

### Google Play

Some microG installers include a patched version of Google Play which can run on microG. Although this is not privacy preserving, it is the only way to get a lot of paid apps and all in-app purchases. These installers include [Nanodroid](wiki/nanodroid) and the [Minified microG Installer](wiki/minmicrog).

### Apkpure

**Not recommended**

[Download from apkpure.com](https://apkpure.com/)

If Google locks the anonymous Aurora anonymous account again, you can try to use this store to update your apps.

It seems that apps are pulled from Play Store, you can hope that they don't modify the apks.