---
layout: page
title:  "Using Reddit with microG"
tags: alternative
---

## Open Source Alternative Reddit Client

### Infinity for Reddit

**[Available on F-droid](https://f-droid.org/packages/ml.docilealligator.infinityforreddit) through the [izzysoft repository](https://apt.izzysoft.de/fdroid/repo?fingerprint=3BF0D6ABFEAE2F401707B6D966BE743BF0EEE49C2561B9BA39073711F628937A)**

Infinity for Reddit is a material design and opensource Reddit app with many features and great looks.

**Note:** Using Infinity for Reddit does not make your interactions on Reddit more private. Everything you do on Reddit can still be tracked monitored and analysed.

### Slide for Reddit

**[Available on F-droid](https://f-droid.org/en/packages/me.ccrama.redditslide)**

Slide for Reddit is an open source and private Reddit client for Android.

**Note:** Using Slide for Reddit does not make your interactions on Reddit more private. Everything you do on Reddit can still be tracked monitored and analysed.



### Redreader

**[Available on F-droid](https://f-droid.org/en/packages/org.quantumbadger.redreader)**

Redreader is an open source and private Reddit client for Android.

**Note:** Using Redreader does not make your interactions on Reddit more private. Everything you do on Reddit can still be tracked monitored and analysed.

## Official Reddit apps on microG


The offical Reddit app is working well, including notifications, etc.