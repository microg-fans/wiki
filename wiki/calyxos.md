---
layout: page
title:  "CalyxOS"
tags: rom
---

[CalyxOS](https://calyxos.org/) is a privacy-focused ROM based on the Android Open Source Project with optional support for microG. It was created by [the Calyx Institute](https://calyxinstitute.org/), a non-profit privacy advocacy organization.  It also includes an integrated backup system called [SeedVault](https://calyxinstitute.org/projects/seedvault-encrypted-backup-for-android), the F-Droid privileged extension for automatic app updates, and a selection of user-installable privacy-friendly apps. CalyxOS is one of the few ROMs that that supports relocking the bootloader to get the full security benefit of Android Verified Boot.

CalyxOS is currently supported on:

* Google Pixel 2 (code name walleye)
* Google Pixel 2 XL (code name taimen)
* Google Pixel 3 (code name blueline)
* Google Pixel 3 XL (code name crosshatch)
* Google Pixel 3a (code name sargo)
* Google Pixel 3a XL (code name bonito)
* Google Pixel 4 (code name flame)
* Google Pixel 4 XL (code name coral)
* Google Pixel 4a (code name sunfish)
* Xiaomi Mi A2 (code name jasmine_sprout)

CalyxOS builds and signs their own version of microG, so updates to GmsCore can only be installed from the Calyx repository, rather than upstream.