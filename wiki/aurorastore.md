---
layout: page
title:  "Aurora Store"
tags: app
---

- [Aurora Store is available on F-droid](https://f-droid.org/en/packages/com.aurora.store/) 
- [Offical Telegram Group](https://t.me/AuroraSupport)

Aurora Store is a great tool for using Android without any Google Apps. With Aurora Store, you can download any app from the Google Play Store without any Google Apps (including Google Play Services and Google Play Store). Aurora Store offers two different login methods, Anonymous and Google Account. Anonymous only supports the download of free apps, whereas Google Account login supports the installation of paid apps purchased on the account.

## Paid and Free Apps

### Paid Apps

Paid apps can be installed through Aurora Store is they have already been purchased on a Google account logged into Aurora Store. If an app is purchased on [play.google.com](https://play.google.com/store/apps) with the same account as is logged into Aurora Store, it can be installed. However a lot of paid apps confirm that they have been purchased before they run. These apps do not work as the Play Store is not present to confirm your purchase.

### In-app Purchases

In-app purchases do not work with the Aurora Store, because the Google Play Store must be present to verify license checks. It is sometimes possible to get around this by restoring an app with in-app purchases from a Titanium backup/oandbackup. The backup must come from a ROM on which the license is validated. This may or may not work.

### Free Apps

Free apps can be downloaded from Aurora Store through both anonymous and google account logins.

## Aurora Services

Aurora Services is a system app which allows Aurora Store and Aurora Droid to install apps in the background bypassing the native installer. Aurora Services can be installed through a [Magisk module zip found on the Gitlab releases page](https://gitlab.com/AuroraOSS/AuroraServices/-/releases) or by installing an APK in the system/priv-app folder. Aurora Services can be enabled in the installation section of both Aurora Store and Aurora Droid.

## Aurora Droid

- [Aurora Droid is available on F-droid](https://f-droid.org/en/packages/com.aurora.store/)

Aurora Droid is an F-droid client with the Aurora Store UI, Aurora Services support and some other features.