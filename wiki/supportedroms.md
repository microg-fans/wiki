---
layout: page
title:  "Supported Custom ROMs"
tags: rom
---

a [ROM](https://www.xda-developers.com/what-is-custom-rom-android/) must support Signature Spoofing to work with microG (so microG can pretend to be Google Play Services). Other ROMs can [be patched]((wiki/patching) but this is a complex procedure. For most users the best and easiest option is a supported ROM. To install microG on one of these ROMs, [it is recommended that you use an installer](wiki/installers).


## Recommended
These are custom ROMs which have been tried and tested by members of the /r/microG community and verified to be working with all functions of microG. Feel free to add to this list with any ROM you have found to work well.


Android 9 / Pie and below:

- [LineageOS for microG](https://lineage.microg.org) (note: includes microG by default)
- [CarbonROM](https://carbonrom.org/)

Android 10 / Q and below:

- [crDroid](https://crdroid.net)
- [ArrowOS](https://arrowos.net/)

## Full list

A complete of all custom ROMs which support signature spoofing and should work with microG can be found on [this XDA Forum](https://forum.xda-developers.com/android/general/index-list-custom-roms-offering-t3557047).