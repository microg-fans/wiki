---
layout: page
title:  "Using Twitter with microG"
tags: alternative
---

## Open Source Alternative Twitter Client

### Twidere

**[Available on F-droid](https://f-droid.org/en/packages/org.mariotaku.twidere/)**

Twidere is an open source and private Twitter client for Android.

**Note:** Using Twidere does not make your interactions on Twitter more private. Everything you do on Twitter can still be tracked monitored and analysed.


## Official Twitter apps on microG


The offical Twitter app is working without any problems, including notfications, etc.