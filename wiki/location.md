---
layout: page
title:  "microG Location Backends"
tags: install
---

>Unified Network Location Provider (UnifiedNlp) is a library that provides Wi-Fi- and Cell-tower-based geolocation to applications that use Google’s network location provider. It is included in GmsCore but can also run independently on most Android systems.

\- [the microG project](https://github.com/microg/android_packages_apps_UnifiedNlp/blob/master/README.md)
## Which location provider should I use?
If it works well enough for you, the default [MozillaNlpBackend](https://f-droid.org/en/packages/org.microg.nlp.backend.ichnaea) should be sufficent. It is open source (both the app and the database) and is contributed to by users. You can (and should) contribute to it using the [Mozilla Stumbler](https://f-droid.org/en/packages/org.mozilla.mozstumbler/) app.

If the MozillaNlpBackend is not accurate enough for you, the [AppleWifiNlpBackend](https://f-droid.org/en/packages/org.microg.nlp.backend.apple) has the best coverage. Although the backend is open source, the database it pull is not.

As it is the only option avalible, the [NominatimGeocoderBackend](https://f-droid.org/en/packages/org.microg.nlp.backend.nominatim) must be used as the address lookup backend.


##Full list
List of backends for geolocation mostly taken from [this github page](https://github.com/microg/android_packages_apps_UnifiedNlp/blob/master/README.md) :

* [AppleWifiNlpBackend](https://github.com/microg/AppleWifiNlpBackend) - Uses Apple's service to resolve Wi-Fi locations. It has excellent coverage but the database is proprietary.
* [Déjà Vu Location Service](https://f-droid.org/en/packages/org.fitchfamily.android.dejavu/) - Offline location provider using data collected by your phone.
* [OpenWlanMapNlpBackend](https://github.com/microg/OpenWlanMapNlpBackend) - Uses OpenWlanMap.org to resolve user location but the NLP backend did not reach release-quality, yet. Users interested in a freely licensed and downloadable database for offline use should stick with openBmap for now - *Last updated in 2015*
* [OpenBmapNlpBackend](https://github.com/wish7code/org.openbmap.unifiedNlpProvider) - Uses [openBmap](https://radiocells.org/) to resolve user location. Community-created, freely licensed database that can optionally be downloaded for offline operation. The coverage [varies from country to country](https://radiocells.org/stats/countries) (it's best in central Europe).
* [MozillaNlpBackend](https://github.com/microg/IchnaeaNlpBackend) - Uses the Mozilla Location Service to resolve user location. The coverage is OK. Only the cell tower database is free.
* [LocalWifiNlpBackend](https://github.com/n76/wifi_backend) - Local location provider for Wi-Fi APs using on-phone generated database.
* [LocalGSMLocationProvider](https://github.com/rtreffer/LocalGSMLocationProvider) - Local opencellid based location provider backend. Has been surpassed by LocalGSMBackend which also has an OpenCellID option - *Last update in 2014*
* [LocalGSMBackend](https://github.com/n76/Local-GSM-Backend) - Local location provider for GSM cells. It works offline by downloading freely licensed database files from Mozilla, OpenCellID, or lacells.db.

List of backends for (reverse) geocoding:

* [NominatimGeocoderBackend](https://github.com/microg/NominatimGeocoderService) - Address lookup backend.