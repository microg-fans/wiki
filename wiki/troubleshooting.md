---
layout: page
title:  "App Troubleshooting Instructions"
tags: issue
---

1. Wipe all data from the app (from the app info screen) and grant the app all permissions. Then restart the app.
2. Ask for help on /r/microG.
3. Find an alternative. [Alternativeto.net](https://alternativeto.net/) is a good place to start.