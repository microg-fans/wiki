---
layout: page
title:  "List of apps that are known not to work"
tags: issue
---

List of reported problem apps in the Github wiki: [Problem apps](https://github.com/microg/android_packages_apps_GmsCore/wiki/Problem-Apps)

Ask questions about an app here, and search the internet first! The main developers have better things to do than searching the web for you.

If you found a non working app, and you are sure it's a problem with microG, you can report it on Github: [Github Issues](https://github.com/microg/android_packages_apps_GmsCore/issues)

##Apps that have been reported on this subreddit:

- [Snapchat](/r/MicroG/comments/aaywty/snapchat/)