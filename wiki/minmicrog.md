---
layout: page
title:  "Minimal microG Installer"
tags: install
---

The minimal microG installer is a simple microG installer. Minimal microG uses official microG APKs so it can be updated from the [microG F-droid repo](wiki/store#wiki_interesting_repos). 

**[Official Minimal microG Installer sources Github](https://github.com/FriendlyNeighborhoodShane/MinMicroG)**

**[Official Minimal microG Installer releases Github](https://github.com/FriendlyNeighborhoodShane/MinMicroG_releases/releases)** (separate repo for some reason)

Beta releases include a [fork of GmsCore](https://github.com/nogoolag) that works with Android 10, **if you use Android 10 make sure you use the latest release**.

It comes in four different editions:

## Standard Edition

The Standard edition contains some proprietary Google Apps and libraries.

### Contents

- microG (GMSCore, GSFProxy)
- Modified Google Play Store by Setialpha (required for in-app purchases and paid apps)
- Selected [UNLP backends](wiki/location) (Dejá vu, LocalGSM, LocalWiFi, Mozilla, Nominatim)
- Aurora Droid (F-droid Client)
- Aurora Services (for background and automatic installation from Aurora Droid / Store)
- Swipe libraries for [AOSP keyboard](wiki/keyboard) (of swipe doesn't work, try [EnhancedIME keyboard](wiki/keyboard#wiki_enhancedime).
- Some Google DRM jars
- Google Sync adapters for Android KitKat to Oreo
- Permission files for all of this
- An addon.d file to backup/restore everything on a rom flash
- Support for [easily granting permissions with `npem` terminal command](wiki/issues#wiki_play_store_.28phonesky.29_has_the_correct_signature_.28not_ticked.29).

### Download

[Download on Github](https://github.com/friendlyneighborhoodshane/minmicrog_releases/releases) (zip named `MinMicroG-Standard-XXXXX-signed.zip`

## NoGoolag Edition

The NoGoolag edition is completley free of any Google or proprietary components other then Droidguard.

### Contents

- microG (GMSCore, GSFProxy)
- Selected [UNLP backends](wiki/location) (Dejá vu, LocalGSM, LocalWiFi, Mozilla, Nominatim)
- Fake Store
- Aurora Store
- Aurora Droid
- Aurora Services
- Permission files for all of this
- An addon.d file to backup/restore everything on a rom flash
- Support for [easily granting permissions with `npem` terminal command](wiki/issues#wiki_play_store_.28phonesky.29_has_the_correct_signature_.28not_ticked.29).

### Download

[Download on Github](https://github.com/friendlyneighborhoodshane/minmicrog_releases/releases) (zip named `MinMicroG-NoGoolag-XXXXX-signed.zip`)

## UnifiedNLP Edition

The Unified NLP edition **is not a microG installer** and ONLY flashes UnifiedNLP. If you want to install microG you want Standard or NoGoolag editions. UnifiedNLP does not require a signature spoofing supported ROM.

### Contents

The things included in the UNLP Edition zip are:

- UnifiedNLP
- Maps APIv1
- Selected [UNLP backends](wiki/location) (Dejá vu, LocalGSM, LocalWiFi, Mozilla, Nominatim)
- Permission files for all of this
- An addon.d file to backup/restore everything on a rom flash
- Support for [easily granting permissions with `npem` terminal command](wiki/issues#wiki_play_store_.28phonesky.29_has_the_correct_signature_.28not_ticked.29).

### Download

[Download on Github](https://github.com/friendlyneighborhoodshane/minmicrog_releases/releases) (zip named `MinMicroG-Backend-XXXXX-signed.zip`)

## Aurora Services Edition

The Aurora Services edition **is not a microG installer** ONLY flashes Aurora Services. If you want to install microG you want Standard or NoGoolag editions. Aurora Services does not require a signature spoofing supported ROM.

### Contents

- Aurora Services
- Permission files for all of this
- An addon.d file to backup/restore everything on a rom flash

### Download

[Download on Github](https://github.com/friendlyneighborhoodshane/minmicrog_releases/releases) (zip named `
MinMicroG-AuroraServices-XXXXX-signed.zip`)

## Installation instructions / order

1. Perform a full wipe of your system.
2. Flash your ROM of choice that supports signature spoofing (follow instructions from your ROM for your device)
3. [Optional, but reccomended] Flash [Magisk](https://github.com/topjohnwu/Magisk/releases)
4. Boot to ROM
5. Boot back to TWRP
7. Flash your chosen edition of the minimal microG installer (download from links above)
8. Reboot to ROM
9. [Set up microG](wiki/index#wiki_install_microg)