---
layout: page
title:  "Other Android privacy tools"
tags: alternative
---

## Exodus Privacy
[Exodus Privacy](https://exodus-privacy.eu.org/en/), a [French non-profit](https://exodus-privacy.eu.org/en/page/who/), develops an app which scans installed Android apps for trackers.

### Exodus Privacy App
The **[official Exodus Privacy app](https://f-droid.org/packages/org.eu.exodus_privacy.exodusprivacy/)** (on F-droid) compares the apps on your phone with a database of scanned apps from the Google Play Store and reports which ones track you. This app is licensed under the [GPLv3.0](https://github.com/Exodus-Privacy/exodus-android-app/blob/master/LICENSE)

### ClassyShark3xodus

**[ClassyShark3xodus](https://f-droid.org/packages/com.oF2pks.classyshark3xodus/)** (on F-droid) uses the Exodus Privacy software locally to scan an app or APK file for trackers.

## Ad / Tracking blockers

Ad and tracking blockers block ads and tracking domains system-wide attempting to prevent them from leaving your phone.

### Blokada (no root required)

**[Blokada](https://f-droid.org/en/packages/org.blokada.alarm/)** (on F-droid) poses as a VPN and attempts to block all unwanted advertising and tracking network traffic leaving your phone. Blokada can not be used in conjunction with a VPN or NetGuard.

### AdAway (root required)

**[AdAway](https://f-droid.org/en/packages/org.adaway/)** (on F-droid) is an app which adjusts the system [hosts file](https://en.wikipedia.org/wiki/Hosts_%28file%29) to block traffic going to tracking and advertising hostnames. AdAway requires root access either through Magisk, LineageOS SU addon or another method.



### DNS Based

DNS based ad / tracker blockers filter out unwanted requests through DNS. Root access is not required.

- [NextDNS](https://nextdns.io/) (customisable, but requires an account and set up)
- [Pi Hole](https://pi-hole.net) (customisable, but requires hardware and set up)
- [AdGuard DNS](https://adguard.com/en/adguard-dns/overview.html) (no customizability, no set up or hardware required)

Any one of these options can be added securely by going to Android Settings > Network and Internet > Private DNS > Custom > Enter the DNS-over-TLS domain of your chosen provider (e.g. `dns.adguard.com` for AdGuard DNS). Despite what some services claim, no app is required. Android Pie+ only.

## App Network Traffic Blockers
App Network Traffic Blockers block all network traffic from chosen apps.

### LineageOS Native
Users of LineageOS or a LineageOS based ROM (including LineageOS for microG) can natively block all network traffic from any app. To do this visit the App Info screen for an app, tap on Mobile Data and Wi-Fi, and disable all access.




###NetGuard
**[NetGuard](https://f-droid.org/en/packages/eu.faircode.netguard)** (on F-droid) poses as a VPN to block all network from chosen apps. NetGuard does NOT require root, but can not be used in conjunction with Blokada or a VPN. NetGuard has a paid ("donate") upgrade to unlock more features such as monitoring network traffic (both what is blocked and isn't blocked) and blocking based on a hosts list (like Blokada) but the core functionality is free.

### AFWall+

**[AFWall+](https://f-droid.org/en/packages/dev.ukanth.ufirewall/)** (on F-droid) uses root functionality to block all network traffic from chosen apps. AFWall+ has a paid ("donate") upgrade to unlock more features but the core functionality is free. It can be used in conjunction with a VPN or Blokada.

## App Permissions
### XPrivacyLua
**[XPrivacyLua](https://lua.xprivacy.eu/)** (Google Play, but license can be activated without GPlay by donating | needs Xposed)

Simple to use privacy manager and successor of XPrivacy. Revoking Android permissions from apps often let apps crash or malfunction. XPrivacyLua solves this by feeding apps fake data instead of real data.