---
layout: page
title:  "OmniROM builds with microG"
tags: rom
---

# OmniROM builds with microG

Like LineageOS for microG, [OmniROM](https://www.omnirom.org/) is now publishing builds with microG built in for select devices. Omni has built microG themselves ~~including [the patches to make UnifiedNLP work on Android10](https://old.reddit.com/r/MicroG/comments/fd7udr/fork_of_microg_gmscore_with_unifiednlp_working_in/)~~ (irrelevant since microG version 0.2.11.202414). Unlike LineageOS for microG, these builds are official.

The devices built with microG are:

- [oneplus7t](https://dl.omnirom.org/oneplus7t/) (OnePlus 7t)
- [oneplus7tpro](https://dl.omnirom.org/oneplus7tpro/) (OnePlus 7t Pro)
- [oneplus6](https://dl.omnirom.org/oneplus6/) (OnePlus 6)
- [oneplus6t](https://dl.omnirom.org/oneplus6t/) (OnePlus 6t)
- [beryllium](https://dl.omnirom.org/beryllium/) (POCOFONE F1)
- [raphael](https://dl.omnirom.org/raphael/) (Redmi K20 Pro)
- [mido](https://dl.omnirom.org/mido/) (Redmi Note 4)


Builds are labelled as MICROG instead of WEEKLY. All are based on Android 10. For installation instructions refer to device specific XDA threads.

If [OmniROM supports your device](https://www.omnirom.org/#devices) but doesn't have builds with microG built in you can still Omni and microG you'll just have to flash [minmicroG on top](wiki/minmicrog) (and not everything is guarenteed to work).