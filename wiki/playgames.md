---
layout: page
title:  "Using Google Play Games with microG"
tags: alternative
---

Support for Google Play Games is [an open issue on Github](https://github.com/microg/android_packages_apps_GmsCore/issues/163) and currently does not work. A [bounty is available](https://www.bountysource.com/issues/36614911-support-for-official-google-play-games-apk) when it gets added.

## Xposed

It is possible to get some games working with the No Play Games Xposed module.

## Backup and restore games

If you backup a game from a GApps ROM using an app like Titanium Backup or [NeoBackup](https://github.com/NeoApplications/Neo-Backup) (formerly OAndBackupX) and restore it on a microG ROM some games will work.