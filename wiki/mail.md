---
layout: page
title:  "GMail on microG"
tags: alternative
---

## Ideal Open Source Alternatives

### K-9 Mail (Mail Client)

- [Available on F-droid](https://f-droid.org/en/packages/com.fsck.k9/)

Private email client. Note that this does not make your emails more secure or private, it just makes how you view them more secure and private.

### Fairemail (email client)

- [Available on F-droid](https://f-droid.org/en/packages/eu.faircode.email/)

Private email client. Note that this does not make your emails more secure or private, it just makes how you view them more secure and private.

### Protonmail (email provider)

- [website](https://protonmail.com/)

Private and secure encrypted email provider.

Advantages over GMail:

- Encrypted and secure
- Not owned by Google
- Private

Disadvantages:

- Costs for some features.

## Use GMail with microG

### Use GMail in any email client

GMail can be used with just about any major email client including K-9Mail and Fairemail (see above).

### App

I am not sure if it is possible to use the GMail app with microG, please update if you know.

-> 28/05/2020 : works fine with Android 10 (notifications and app)

### Mobile Web Site / GApps Browser

The GMail mobile website works fine through microG in any browser, or can be accessed through the [GApps Browser app](https://f-droid.org/en/packages/com.tobykurien.google_news/).