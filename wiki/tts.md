---
layout: page
title:  "Google Text-to-Speech Engine on microG"
tags: alternative
---

## Ideal Open Source Alternatives

### RHVoice

**[Avalible on F-droid](https://f-droid.org/en/packages/com.github.olga_yakovleva.rhvoice.android//)**

A high-quality open source TTS engine that works in a few major languages, including US English.


### Flite

**[Avalible on F-droid](https://f-droid.org/en/packages/edu.cmu.cs.speech.tts.flite/)**

Free and open source android text to speech engine. Doesn't sound as good as Google, nor does it support as many languages or look as good, but it does the job. You must download your language's file within the app.



## Use Google Text-to-Speech Engine with microG

### App

**[Download from Play / Aurora Store](https://play.google.com/store/apps/details?id=com.google.android.tts)**

Google made, closed source, and probably privacy infringing. But it works fine with microG. You can disable telemetry data collection settings and if you want better privacy block internet access with [an android privacy utility](wiki/privacytools).