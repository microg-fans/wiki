---
layout: page
title:  "microG Installers"
tags: install
---

If you want to use microG on a custom ROM other then [LineageOS for microG](wiki/lineage4microg), an installer is the easiest and simplest way to install it. A microG installer can be flashed (from your custom recovery or Magisk) in place a GApps package in the instructions to install your chosen [supported ROM](wiki/supportedroms). For it to work, your ROM must support Signature Spoofing; if it doesn't, you have to [patch it](wiki/patching) to make it work, which is a much more complex process. Some [ROMs which support signature spoofing and microG can be found on this page](wiki/supportedroms). It is also advisable to use a ROM that doesn't bundle GApps by default, as they generally need to be uninstalled beforehand.

## microG Installer Revived

A simple and lightweight microG installer that can be flashed on Magisk. Simple microG which installs the main microG components, as well as various [UnifiedNLP Backends](wiki/location), as a system app. Available on the [Magisk Modules Repository](https://magisk.download/microg-installer-magisk-module-repository/) (more info and instructions on [Github](https://github.com/nift4/microg_installer_revived)).

## Unofficial microG Installer

**[Website](https://github.com/micro5k/microg-unofficial-installer)**

Another simple installer that installs the key components of microG, available on [XDA](https://forum.xda-developers.com/showthread.php?t=3432360).

## minmicroG

**[See the wiki page on minmicroG for more info](wiki/minmicrog)**

Installs microG and Aurora Store, Aurora Droid, Aurora Services and various UnifiedNLP Backends. Optionally, you can install the standard edition, which also includes [Patched Google Play Store](wiki/patchedplay) and some other proprietary Google bits and pieces (including AOSP swipe keyboard, some DRM jars and sync adapters), or more basic editions. See the [minified microG installer wiki page](wiki/minmicrog) for more information, download links and installation instructions. While it can take care of the software stores, take note that, as of July 2022, this installer isn't frequently maintained, which might be problematic if you want to use Aurora Store.

## Nanodroid

**[See the wiki page on nanodroid for more info](wiki/nanodroid)**

The most customisable installer. If you want a lot of options and know what you're doing look here. The most prominent and popular of microG installer is [Nanodroid](https://gitlab.com/Nanolx/NanoDroid) which installs microG along with many other FOSS utilities (you can choose exactly what it installs using the Setup Wizard). More information [on the Nanodroid page here](wiki/nanodroid).