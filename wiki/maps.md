---
layout: page
title:  "Google Maps on microG"
tags: alternative
---

## Ideal Open Source Alternatives

### OsmAnd

- [Avalible on F-droid](https://f-droid.org/en/packages/net.osmand.plus/)

Offline OpenStreetMap client. Includes turn by turn navigation and complex map viewing.

Advantages over Google Maps:

- Free, open source and private
- Fully offline, no internet required

Disadvantages over Google maps:

- Can be slower rendering maps
- No traffic information in directions

OsmAnd and other OpenStreetMap clients rely on you to help update the maps. This can be easily with **[StreetComplete](https://f-droid.org/en/packages/de.westnordost.streetcomplete/)**.

If you did not install Google Apps, you will probably have no Text to Speech (TTS) provider. That means you won't have spoken navigation. Take a look at [this page on Text-to-Speech Engines to find one right for you](wiki/tts)

### Transportr (Public Transport)

- [Avalible on F-droid](https://f-droid.org/en/packages/de.grobox.liberario/)

Open source and private public transportation app.

### Offi Stations (Public Transport)

- [Avalible on F-droid](https://f-droid.org/en/packages/de.schildbach.oeffi/)

Open source and private public transportation app.

### Magic Earth

**Note: Although Magic Earth has a solid privacy policy, it is closed source so nothing can be verified**

- [Get it on Google Play / Aurora Store](https://play.google.com/store/apps/details?id=com.generalmagic.magicearth)

OpenStreetMap powered maps app with accurate turn by turn navigation taking into account traffic and smooth map browsing. Probably the best (feature wise, not security / privacy) OpenStreetMap app for navigation and normal use.

## Use Google Maps with microG

### App

It is possible to use Google Maps with microG. Open Google Maps, go to settings and then select sign out. You can then Google Maps signed out using microG. It will frequently warn you that your Google Play Service is out of date, ignore this and it should work fine.

### Mobile Web Site / GApps Browser

The Google Maps mobile website works fine through microG through any browser, or and can be accessed through the [GApps Browser app](https://f-droid.org/en/packages/com.tobykurien.google_news/).