---
layout: page
title:  "Patch a ROM for Signature Spoofing"
tags: install
---


If you wish to use microG on a ROM which does not already support signature spoofing. If you do not know what you are doing, have a look at the [list of supported ROMs](wiki/supportedroms). Patching a ROM is a complex process and not recommended unless you know what you are doing, if it is avalible using a ROM which is already supported is a much simpler option.

## Deodexing services.jar instructions

These instructions are taken from the [unofficial microG Telegram group](https://t.me/NoGoolag). This is **NOT required for using either Nanodroid or microG** and is only needed if you want to patch an unsupported ROM with Nandroid patcher.

Deodexing services.jar (Tried on Android Pie)

REQUIRED: MacOS or Linux machine (Cygwin, windows, or whatever you have may not work)

♦ Checking

1. In TWRP find /system/system/framework/services.jar and pull it to your PC

2. Extract it using "unzip" or "jar -xvf"

3. Check if the jar file has classes.dex
-- IF IT DOES - You don't need this, use Nanodroid Patcher directly
-- IF IT DOESN'T - Continue

4. Go to /system/framework/oat/[your arch] or if that doesn't exist /system/system/framework/oat/[your arch]

5. Check for "services.vdex". if you have this, you can continue. Otherwise, you're screwed

♦Preparation for patching

6. Make a new directory somewhere and enter it via git.

7. Clone vdex Extractor from here: https://github.com/anestisb/vdexExtractor

8. Enter vdex Extractor and follow the instructions on the github page to compile it (I am not spoonfeeding you guys)

9. Now, pull the framework folder from your phone by doing "adb pull /system/framework" or if that gives nothing "adb pull /system/system/framework"

10. Open the framework folder and copy services.jar somewhere safe as a backup

11. Create a new folder named "input" and make another copy of services.jar in there

♦ Extracting vdex

12. Put the vdexExtractor bin into the main directory, or perhaps /usr/bin directory or /usr/local/bin (depends on your OS) so you can execute the bin from anywhere

13. Copy services.vdex from the framework folder (step 4) to the main folder

14. Extract it using
"vdexExtractor -i services.vdex"

15. You should get a services_classes.cdex file

♦ Converting cdex to dex
(These instructions are very vague on the github for vdex extractor, so I'm fixing your problems here)

16. Go to this link:
https://github.com/anestisb/vdexExtractor#compact-dex-converter
and download a standalone build for your system (I used MacOS). 

17. Extract and copy the [compact dex converter] folder to the main folder

18. cd into [compact dex converter]/bin

19. Execute ./compactdexconverter ../../services_classes.cdex

20. This should create a services_classes.cdex.new file (This is the .dex file, they don't rename it to .dex for some reason). Rename this file to classes.dex

21. Copy classes.dex into the input folder.

22. Now type "zip -j services.jar classes.dex"

23. Push input/services.jar to /system/system/framework

24. DONE, now flash nanodroid patcher

## Nanodroid Patcher

The Nanodroid patcher can patch a ROM that does not support signature spoofing to support signature spoofing. Nanodroid patcher requires a deodexed ROM (instructions to do this below) Information on the Nanodroid patcher can be found on the [Nanodroid Gitlab page and is only reccomended for advanced users](https://gitlab.com/Nanolx/NanoDroid). For non-advanced users it is best to use a [ROM which supports signature spoofing](wiki/supportedroms).

## Smali Patcher

https://forum.xda-developers.com/apps/magisk/module-smali-patcher-0-7-t3680053

**WARNING:**


- https://forum.xda-developers.com/showpost.php?p=80287799
- https://forum.xda-developers.com/showpost.php?p=80292041
- https://forum.xda-developers.com/showpost.php?p=78958124


## Needle, Haystack and Tingle

### Haystack

Nice, simple tool by Lanchon. Major props!

&nbsp;

Requirements:

* Basic knowledge (adb, twrp, shell commands).
* A GAPPS-less ROM installed to your phone.
* [Haystack](https://github.com/Lanchon/haystack) - this is where the magic happens; a tool to patch your ROM for signature spoofing.
* [A microG installer](wiki/installers).
* A PC, preferably running a Linux distro (a VM should work fine, too).

Steps:
In the following example, I'll be referring to a Galaxy S6 running Android version 9.0.

*On your PC:*

1. Download [Haystack](https://github.com/Lanchon/haystack): either the `.zip` file, or via `git clone https://github.com/Lanchon/haystack.git`.
2. Install the required dependencies (JRE).
3. On your phone: boot to TWRP, and mount the `system` partition. 
4. On your PC, use the `pull-fileset` script from Haystack: `./pull-fileset fs-s6`.
5. Patch the fileset with the appropriate hook for your version of Android - in this case, Pie (9.0) corresponds to `sigspoof-hook-7.0-9.0`. Also note the API level for your Android version - in this case, 28: `./patch-fileset patches/sigspoof-hook-7.0-9.0/ 28 fs-s6/`. It will output to `fs-s6__sigspoof-hook-7.0-9.0`.
6. Patch your freshly-patched fileset to add the core patch: `./patch-fileset patches/sigspoof-core/ 28 fs-s6__sigspoof-hook-7.0-9.0`. It will output to `fs-s6__sigspoof-hook-7.0-9.0__sigspoof-core`.
7. Add the UI patch: `./patch-fileset patches/sigspoof-ui-global-9.0/ 28 fs-s6__sigspoof-hook-7.0-9.0__sigspoof-core/`. The fully-patched files are now in `fs-s6__sigspoof-hook-7.0-9.0__sigspoof-core___sigspoof-ui-global-9.0`.
8. Push the fileset to the device: `./push-fileset fs-s6__sigspoof-hook-7.0-9.0__sigspoof-core__sigspoof-ui-global-9.0`.
9. Reboot your phone to system.

*On your phone:*

1. Enable developer options.
2. Scroll all the way to the bottom; enable Signature Spoofing. 
3. Verify that it works using [Lanchon's app](https://github.com/Lanchon/sigspoof-checker).
4. Reboot to TWRP.
5. Flash the unofficial microG installer. 

&nbsp;

That's pretty much it. This method was tested with a Galaxy S6, running unofficial LineageOS 16.0.