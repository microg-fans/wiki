---
layout: page
title:  "Using Facebook with microG"
tags: alternative
---

## Open Source alternatives

### Manyverse

Open source (MPL 2.0 license), decentralised social network. It works like a blockchain. Still in beta. Available on the F-Droid and Play Store. https://www.manyver.se/ 

## Open Source Alternative Facebook Client

### Frost for Facebook

**[Available on F-droid](https://f-droid.org/en/packages/com.pitchedapps.frost)**

Frost for Facebook is an open source and private fully functional Facebook mobile web wrapper with support for both Facebook and Messenger.

**Note:** Using Frost for Facebook does not make your interactions on Facebook more private. Everything you do on Facebook can still be tracked monitored and analysed.

## Official Facebook apps on microG

### Whatsapp

Whatsapp works fine on microG

### Messenger

Facebook Messenger works fine on microG

### Instagram

Instagram works fine on microG

### Facebook

Facebook works fine on microG.