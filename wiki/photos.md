---
layout: page
title:  "Google Photos on microG"
tags: alternative
---

## Ideal Open Source Alternatives

### Simple Gallery Pro

**[Available on F-droid](https://f-droid.org/en/packages/com.simplemobiletools.gallery.pro/)**

A simple but fully featured and open source gallery app. Does not support any sync or backup.

Advantages over Google Photos:

- Open source, offline and private

Disadvantages over Google Photos:

- No photo scanning / search
- Less powerful editing
- No sync or backup
- No optimise phone storage option

### Syncthing-fork

**[Available on F-droid](https://f-droid.org/en/packages/com.github.catfriend1.syncthingandroid/)**

A peer-to-peer syncing app to send files (including all photos taken from your phone) from your phone to your computer.

## Use Google Photos with microG

The app works. However, it cannot automatically back up photos. Manual upload of pictures works, but is flaky. Google Photos can be installed as a Progressive Web Application by "installing" it from Chrome(ium)/Bromite or newer versions of Firefox Preview (Nightly, currently). This allows use of Google Photos without having the app on your phone. Pictures can be uploaded manually and reliably through the web app.

Google Photos with automatic backup apparently works if you restore the app (and related app data) from a backup of Google Photos running with GApps.