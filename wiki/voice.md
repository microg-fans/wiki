---
layout: page
title:  "Google Voice on microG"
tags: alternative
---


## Ideal Open Source Google Voice Alternatives

[JMP.CHAT](https://jmp.chat/):  This uses XMPP (Jabber) for voice, sms, video and interfaces with standard PBX phone numbers.  It's not free, but it is fairly cheap.  Google Voice originally used XMPP before Google made it more proprietary.

##Using Google Voice on microG

The app works but push notifications don't always work.

[See more in this post](/r/MicroG/comments/epzuu1/does_google_voice_work_with_microg/)