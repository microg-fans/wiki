---
layout: page
title:  "Google Password Manager on microG"
tags: alternative
---

## Ideal Open Source Alternatives

### Bitwarden

- **[Official Website](https://bitwarden.com/)**
- **[Bitwarden Official F-droid Repo](https://mobileapp.bitwarden.com/fdroid/)**

Bitwarden is a free, open source, private and fully featured password. With a free account, Bitwarden can store and sync passwords accross multiple devices and multiple platforms and autofill them into different apps including your browser on Android.

## Use Google Password Manager with microG

unknown