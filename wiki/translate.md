---
layout: page
title:  "Google Translate on microG"
tags: alternative
---

## Alternatives

### Deepl Translater

**[Official Website](https://www.deepl.com/translator)**

DeepL is an alternative to Google Translate with a good privacy policy and claims to be "more accurate". A more limited number of languages avalible. DeepL is closed source.

### Apertium

**[Official website](https://www.apertium.org)**

Apertium is an open source, private and secure online translation service.

### Swisscows Translate

**[Official Website](https://swisscows.ch/translate)**

Online translate service by Swiss private search engine Swisscows. Powered by Yandex.

### Mitzuli

**[Avalible on F-droid](https://f-droid.org/app/com.mitzuli)**

Mitzuli is an offline Android translation service. Fully open source and private.

## Use Google Translate with microG

### App

Most functions of the Google Translate app work with microG.

### Mobile Web Site / GApps Browser

The Google Translate mobile website works fine through microG in any browser, or can be accessed through the [GApps Browser app](https://f-droid.org/en/packages/com.tobykurien.google_news/).