---
layout: page
title:  "Google Messages / Hangouts on microG"
tags: alternative
---

## Ideal Open Source Alternatives

### Signal

**Available on the Google Play Store (download through [Aurora Store](wiki/store))**

**[Official auto-updating APK](https://signal.org/android/apk/)** *for people who cannot install the app through the Play Store*

[Unofficial F-droid repo](https://gitlab.com/rfc2822/fdroid-firefox) *maintained by Ricki Hirner; includes Signal and various other apps*

End-to-end encrypted and open source instant messaging program

Advantages over Google Messages:

- Free, open source and private
- End-to-end encrypted
- Can run without Google Play Services

Disadvantages over Google messages:

- Not as many users

**Use without Play Services:** If you want to use Signal without Play Services (or not have it register to Google Cloud Messaging through MicroG), make sure that you turn off Google Cloud Messaging in microG settings (or require apps to ask before registration) before you install the Signal APK. Signal will automatically detect if it is on a device that does not have Play Services, and instead use a persistent background connection. Note that this can drain your battery somewhat faster.

### QKSMS

**[Available on F-droid](https://f-droid.org/en/packages/com.moez.QKSMS/)**

An open source text messaging (SMS / MMS) app with a slick UI. Note that is just for text messages, no added privacy or encryption is added.

## Use Google messaging services with microG
### Use [Hangouts](https://play.google.com/store/apps/details?id=com.google.android.talk&hl=en_US) with MicroG

*Note: Google is planning on shutting down Hangouts by the end of 2020.*

The official Hangouts app does not work well on MicroG. However, you can still connect to [Hangouts over XMPP](https://github.com/linuxcsuf/linuxcsuf/wiki/XMPP-with-Google-Hangouts). The excellent [Conversations](https://f-droid.org/en/packages/eu.siacs.conversations/) XMPP client is recommended.

In Conversations, go to Settings -> Expert Settings and enable Extended Connection Settings. Then, create a new account with these parameters:

 - XMPP address: username@gmail.com
 - Hostname: talk.google.com
 - Port: 5222
 - Security: SSL

Note that XMPP attachments will not work with other users. Hangouts attachments seem to be delivered as links. Hangouts group chat does not work.

### Use [Google Messages](https://play.google.com/store/apps/details?id=com.google.android.apps.messaging&hl=en_US) with microG

Google Messages seems to support basic SMS communication, but does not successfully pair with the Messages for Web feature.