---
layout: page
title:  "Backing up your phone with microG"
tags: alternative
---

## Syncthing

- [Available on F-droid](https://f-droid.org/en/packages/com.github.catfriend1.syncthingandroid)

Syncthing allows you to transfer any files from your phone to your computer (or any other device) easily and securely over peer-to-peer, so no external services are needed. You can transfer your entire phones storage, including all photos, downloads and other files but not apps and app data unless it has been backed up by Oandbackup. You can restore backups through syncthing (slow) or through USB (fast).

## OandbackupX (successor to Oandbackup)

- [Available on F-droid](https://f-droid.org/en/packages/com.machiav3lli.backup/)

Oandbackup allows to backup any and all apps and app data to a file which can then be moved from your computer using Syncthing, USB or any other means. It works for most apps, but not all especially apps like Signal or Bitwarden which take security very seriously. There is also another alternative called Titanium backup which is not open source and costs for pro features (while Oandbackup is free and open source). You can backup oandbackups through syncthing or through USB.

## TWRP

- [Official Website](https://twrp.me/)

If you are a user of the TWRP Recovery it can back up your entire system. TWRP lets you select which partitions you want to backup, out of: boot, sytstem, data, cache, modem & EFS. Boot, system & data give you a snapshot of your phone, with all settings and apps, which is then saved to the storage partition. You can then restore directly onto your phone - so you can hop between ROMs and test configurations. It also lets you backup to USB - [USB OTG adapters](https://www.mymemory.co.uk/mymemory-usb-otg-adapter.html) are amazingly useful (not just for the fun of plugging a mouse in and seeing the little mouse pointer move about your phone's screen).

You can then backup the storage partition by just copying files directly to a PC, from recovery, provided you have the ADB drivers installed.