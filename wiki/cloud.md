---
layout: page
title:  "Google Drive (Cloud / Sync) on microG"
tags: alternative
---

## Ideal Open Source Alternatives

### Syncthing-fork

**[Avalible on F-droid](https://f-droid.org/en/packages/com.github.catfriend1.syncthingandroid/)**

A peer-to-peer syncing app to send files (including all photos taken from your phone) from your phone to your computer. One folder on your phone can be constantly synced with one folder on any other device securely, similar to Dropbox but without any external companies or servers handling your data. Requires [syncthing desktop client](https://syncthing.net/) to sync with desktop.

### Nextcloud (self-hosted)

**[Many apps avalible on F-droid](https://search.f-droid.org/?q=nextcloud&lang=en)** | [Official Website](https://nextcloud.com)

Nextcloud is an easy-to-use cloud, privacy focused, and a fantastic alternative to any cloud like Google drive. It supports 2fa, remote wipe, military grade encryption and other privacy-centric features. It is used by millions of people and many enterprises and can be an alternative for the majority of Google services.
Nextcloud is a self-hosted, open source and private online drive service. Once set up, you can use for pretty much everything Google Drive does. Nextcloud has many [apps](http://apps.nextcloud.com/), developed by them and by third-parties. They can solve any need: from a video conference platform to a music player and a Google Docs alternative. Many providers offer from 2GB to 5GB for free, [sign up here](https://nextcloud.com/signup).
### DavX^5

**[Avalible on F-droid](https://f-droid.org/en/packages/at.bitfire.davdroid/)**

DavX^5 enables shared calendar and contacts sync other the CalDav and CardDav protocols from most online calendar services. [List of tested compatible services](https://www.davx5.com/tested-with).

**[Instructions to use DavX^5 to sync can be found on this other wiki page](wiki/contacts)**

## Use Google Drive with microG

### App

The Google Drive works fine on microG.

### Mobile Web Site / GApps Browser

The Google Drive mobile website works fine through microG in any browser, or can be accessed through the [GApps Browser app](https://f-droid.org/en/packages/com.tobykurien.google_news/).