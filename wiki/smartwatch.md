---
layout: page
title:  "Smartwatches on microG"
tags: alternative
---

## Android Wear on microG

Android Wear does not yet work with microG. [There is an open issue for it](https://github.com/microg/android_packages_apps_GmsCore/issues/4) and it currently has an [$85 bounty](https://www.bountysource.com/issues/6460438-wishlist-com-google-android-gms-wearable).

## Gadgetbridge

**[Avalible on F-droid](https://f-droid.org/en/packages/nodomain.freeyourgadget.gadgetbridge/)**

Gadgetbirdge is a free, open source and private app to privately use a number of smartwatches including Pebble, Mi Band, Amazfit Bip, Hplus devices and more.

## AsteroidOS

**[Official Website](https://asteroidos.org)**

AsteroidOS is an open source alternative firmware to Google's WearOS compatible with some WearOS smartwatches.