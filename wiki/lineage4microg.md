---
layout: page
title:  "LineageOS for microG"
tags: rom
---

It is recommended for beginners to start off with [LineageOS for microG](https://download.lineage.microg.org/) as this version comes with microG pre-installed. It will not require any additional setup, and it will still have the features that an official LineageOS ROM will have, such as OTA updates that are based on the actively maintained [LineageOS website](https://lineageos.org/), Privacy Guard, etc. The installation process will be the same as an official LineageOS ROM.

NOTE: do *not* ask for support about LineageOS for microG in the official LineageOS subreddit. They do not support microG (and sometimes like to pretend it doesn't exist). Ask for support in /r/microG.

## Installation

Installing LineageOS for microG is the same as normal LineageOS, just use the microG ROM (found [here](https://download.lineage.microg.org/)) and do not flash a GApps package. Installation instructions and guides can be found on Youtube, from a web search and on the [LineageOS Wiki](https://wiki.lineageos.org/).

### How to find if your device has LOS4microG, and which rom to download?

1. Find your device on the regular LOS website. If your device has official LOS support, than it should have LOS4microG as well. Find your device in the list on this site: https://download.lineageos.org/
2. Find the codename of your device. You can see this codename as the last part of the name of the rom or next to the phone name with grey letters on the previous website. 
3. Now go to LOS4microG download site, and you can find your phone by it's codename: https://download.lineage.microg.org/

## Things to consider

### Updating microG

It is safe to update microG from F-Droid. The built-in F-Droid app store detects microG Services Core as "UnifiedNlp (No GAPPS)" due to package naming conflicts. You will likely have an update to this package when first installing LineageOS for microG. The microG versions are in the 0.2.X range, so check the version numbers on this package. At the time of writing, the latest microG services core version is 0.2.10.X. Note that [Aurora Droid](https://f-droid.org/en/packages/com.aurora.adroid/) is able to properly display the package name.

### Text to Speech (TTS)

When no GApps are flashed, LineageOS does not have a text-to-speech (TTS) provider. This means that, for example, spoken directions from navigation apps will not work. There are unfortunately no working open source TTS providers on Android at the moment. Use [Google's TTS](https://play.google.com/store/apps/details?id=com.google.android.tts&hl=en_US). You will have to turn on the TTS provider in Android settings after downloading it.

## Bug Reporting

[](#bugreporting)