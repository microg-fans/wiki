---
layout: page
title:  "Common microG Issues"
tags: issue
---

This is a collection of common issues with microG. If anything in your microG self check is not ticked, this is the place to look.

## Play Store (Phonesky) has the correct signature (not ticked)

To solve this, you need to run two commands in Terminal (built in app on many custom ROMs, may need to be enabled, search for it in settings) or [Termux](https://f-droid.org/en/packages/com.termux/) (free app on F-droid). Push enter after each line, do not include the number.

1. `su`
2. `pm grant com.android.vending android.permission.FAKE_PACKAGE_SIGNATURE`

or if you use **Nanodroid** or **minmicroG 2.6+** (downloaded after Febuary 2020) ([this does not work with any other microG installer](https://gitlab.com/Nanolx/NanoDroid/blob/master/doc/NanoDroidPerm.md)),

1. `su`
2. `npem`

## UnifiedNlP do not have Location to test Geocoder (not ticked)

To solve this, you need to run four commands in Terminal (built in app on many custom ROMs, may need to be enabled, search for it in settings) or [Termux](https://f-droid.org/en/packages/com.termux/) (free app on F-droid). Push enter after each line, do not include the number.

1. Download [this file from nanodroid gitlab](https://gitlab.com/Nanolx/NanoDroid/-/raw/master/Full/system/xbin/nanodroid-perm?inline=false) (you do need need to be on nanodroid to use this).
2. Run the following command in Terminal / Termux:

> `eval "$(cat "/sdcard/Download/nanodroid-perm.txt")"`

3. If it doesn't work check that the file is downloaded as `nanodroid-perm.txt` in the `Downloads` folder. Otherwise make a post here.

or if you use **Nanodroid** or **minmicroG 2.6+** (downloaded after Febuary 2020) ([this does not work with any other microG installer](https://gitlab.com/Nanolx/NanoDroid/blob/master/doc/NanoDroidPerm.md)),

1. `su`
2. `npem`

## UnifiedNLP is not registered in system

This is normally because of proprietary qualcomm locaton app built into your ROM. This can often be solved with these terminal commands. To solve this, you need to run two commands in Terminal (built in app on many custom ROMs, may need to be enabled, search for it in settings) or [Termux](https://f-droid.org/en/packages/com.termux/) (free app on F-droid). Push enter after each line, do not include the number.

1. `su`
2. `novl -a com.qualcomm.location`

## Location Backend(s) are not set up correctly

See the [Location Backends page for more info](wiki/location)

## Some features do not work

Grant all possible permissions to microG in Android permissions, PrivacyGuard (LineageOS), AppOps or any other permissions manager you use.

## Add Google account fails after the first screen

Make sure microG is updated to at least version 0.2.7.17455

##Battery Drain

microG fails to register applications to GCM (Google Cloud Messaging) if they were installed before microG, but the apps keep trying to register and that causes the battery drain, all apps installed after microG are properly registered, to fix the battery drain either. Solutions:

-  Do a clean flash of your ROM (, Magisk) and NanoDroid and install your apps after microG setup
-  Uninstall and re-install all your applications (backup application data if required)

## Google Cloud Messaging (GCM) current state is disconnected in microG settings


Dial `*#*#2432546#*#*` into your dialer app. [More info](https://github.com/microg/android_packages_apps_GmsCore/issues/740#issuecomment-479252876).

## Apps that use MapsAPIv2 (display Google Maps inside their app, like car/bike sharing apps often do) display an empty map under Android 10

~~Sadly, this known issue does not yet has a fix. It is being worked on and there are several related github issues 
[Location permission problems](https://github.com/microg/android_packages_apps_GmsCore/issues/986)
[System doesn't support location provider](https://github.com/microg/android_packages_apps_GmsCore/issues/1010)
and some more...~~ Update microG to version 0.2.202414

## NanoDroid: "System grants Signature Spoofing permission" not ticked, but "System spoofs signature" ticked

Looks strange, but everything works. It's fine.

## Another issue

[Have a look through this list of other common issues on XDA](https://forum.xda-developers.com/apps/magisk/module-nanomod-5-0-20170405-microg-t3584928/post79468220#post79468220), if it isn't there ask for support:


#### [Make a post on /r/microG](/r/microG/submit?selftext=true)

- Ask about anything, help, bug reports and anything in between.
- Please make sure to include all relevant information, what ROM you are using, what app version, instructions to reproduce etc/
- Do *not* ask for support about LineageOS for microG in the official LineageOS subreddit. They do not support microG (and sometimes like to pretend it doesn't exist). Ask for support in /r/microG.

#### [Unofficial microG Telegram Group](https://t.me/NoGoolag)

- Place to chat if you need help
- Ask anything