---
layout: page
title:  "Sync Google Contacts and Calendar on microG"
tags: alternative
---

## Offline Onetime Backup: 
The fastest, easiest and most privacy-friendly way is to just backup your vcf and restore it on each wipe. Not much for convenience, but we wouldn't be here if that bothered us, would we? :)

### To and From Contacts:

1. Go to contacts app 
2. For Backup, Options > Import/Export > 'Export to .vcf file' 
3. The .vcf file will be saved in the root of your storage. Keep it secure. Or I'll come for it.
4. For Restore, Options > Import/Export > 'Import from .vcf file' 

### From Google:

1. Go to https://contacts.google.com/ and login
2. Use sidebar > Export > select 'Export as VCard' > Export

## With CardDAV and DavX^5:

You can sync contacts using the open-source CardDAV client DavX^5 (**[Available on F-droid](https://f-droid.org/en/packages/at.bitfire.davdroid/)**)

1. First of all, go to https://www.google.com/settings/security/lesssecureapps
With your account and enable the setting
2. When logging in with DAVDroid, Use "Login with URL and user name"   
   -- Base URL: https://www.google.com/calendar/dav/your_gmail_id@gmail.com/events  
   -- User name: your_gmail_id@gmail.com  
   -- Password: Your Google account password  

The URL might change over time.

## Exchange ActiveSync
1. Install google-mail, dissallow all permissions except calendar sync
2. add the exchange account to google-mail

The exchange account will now be synced to the systemwide internal calendar and can be used in any calendar app.