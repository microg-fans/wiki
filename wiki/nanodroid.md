---
layout: page
title:  "Nanodroid"
tags: install
---

**[Official Nanodroid Gitlab page](https://gitlab.com/Nanolx/NanoDroid)**

>NanoDroid is a installer for various OpenSource related things, most noticably microG and F-Droid. It supports direct /system installation, both devices with or without A/B partition scheme, aswell as Magisk Mode (module) installation. It also includes several tools (eg. GNU Bash, GNU Nano, more), scripts and additional features (system debloating, init scripts, automatic logcat creation), aswell as a companion F-Droid Repository.

[Nanodroid can be downloaded here](https://downloads.nanolx.org/NanoDroid/Stable/) (the largest zip at the top of the page). **New 2020:** If you want the patched play store, google sync libraries or google swipe libraries you must also download the Nanodroid-Google zip from the same page.

## Nanodroid Setup Wizard

To control what Nanodroid installs, the Nanodroid setup wizard is required. Without the setup wizard Nanodroid installs a large number of [open source utilities and apps as system apps](https://gitlab.com/Nanolx/NanoDroid/blob/master/doc/Applications.md). Most people probably do not want all of these apps, so it is reccomended that you use the setup wizard. If you do not want every app, you need to flash the Nanodroid-setup wizard zip before flashing Nanodroid (but after flashing Magisk). The setup wizard is avalible [on the Nanodroid download site](https://downloads.nanolx.org/NanoDroid/Stable/) named nanodroid-setupwizard...


Nanodroid Setup Wizard only works on arm and arm64, if your device is x86 or x86_64, it won't work. If the Setup Wizard does not work, you have to make [a configuration file](https://gitlab.com/Nanolx/NanoDroid/blob/master/doc/AlterInstallation.md) to customise Nanodroid.

## Installation instructions / order

1. Perform a full wipe of your system.
2. Flash your ROM of choice that supports signature spoofing (follow instructions from your ROM for your device)
3. [Optional, but reccomended] Flash [Magisk](https://github.com/topjohnwu/Magisk/releases)
4. Boot to ROM (if you flashed Magisk)
5. Boot back to TWRP
6. [Optional] Flash Nanodroid-setupwizard and select what you want.
7. Flash the main Nanodroid file (largest zip found on the [downloads page](https://downloads.nanolx.org/NanoDroid/Stable/))
8. [Optional] Flash the Nanodroid-Google zip (found on the [downloads page](https://downloads.nanolx.org/NanoDroid/Stable/))
8. Reboot to ROM
9. [Set up microG](wiki/index#wiki_install_microg)

## Nanodroid-patcher

See the [Patch a ROM for microG](wiki/patching) page for more information.