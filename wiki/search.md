---
layout: page
title:  "Google Search on microG"
tags: alternative
---

## Ideal Private Alternatives

### see [privacyguides.org search engine guide](https://www.privacyguides.org/en/search-engines/)

#### [DuckDuckGo](https://duckduckgo.com)
#### [Qwant](https://qwant.com)
#### [Searx](https://searx.me/)


All work with any [web browser](wiki/browser).

## Use Google with microG

Google works with any [web browser](wiki/browser) using microG.