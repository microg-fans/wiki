---
layout: page
title:  "Patched Play Store"
tags: install
---

The patched play store is a modded version of the Google Play Store to allow it to work with microG. It is still closed source and not private, but allows for in app purchases and paid apps not possible through [Aurora Store](wiki/aurorastore). [Gitlab page for mods](https://gitlab.com/Nanolx/microg-phonesky-iap-support).

## How to get the Patched Play Store

The patched play store in included in the [Nanodroid](wiki/nanodroid) (must be configured) or [minmicrog standard edition](wiki/minmicrog) [installers](wiki/installers).

## How to fix blank screen, infinite loading, or RH-01 error

- Go to System > Apps > Play Store
  - Force stop
  - Clear storage
  - Grant all permissions (make sure sigspoof is) including [granting signature spoofing permissions again](wiki/issues#wiki_play_store_.28phonesky.29_has_the_correct_signature_.28not_ticked.29)

If even that doesn't work:

- Settings > System > Accounts > Remove Google Account
- System > Accounts > Disable Account Data Syncronization
- After all is set in microG self-test after doing the above steps, open Play Store and re-add your account.
- You may have to close (recent apps > swipe it away) the Play Store once - et voila, it works properly again.

## Updating

Updates can be got from the [Nanodroid F-droid repo](https://nanolx.org/nanolx/nanodroid).