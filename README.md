# microG wiki

Backup of the community wiki from reddit.

Original url: https://old.reddit.com/r/MicroG/wiki

## Disclaimer

**Neither this wiki or contributor to it is responsible for bricked devices, dead SD cards, thermonuclear war, or you getting fired because the alarm app failed. If you follow instructions carefully this should not happen, but every change or modification you make is your choice and your responsibility. That being said if you think you have any issues please ask for help.**

Also note that your warranty may be void if you tamper with any part of your device / software. Refer to your phone's manufacturer for any more information on this.


## This is a backup

Date of backup: 2023-06-25

Some information may be outdated! Some links may not work! Hopefully this will be solved in the future in the new home of this wiki.

Original authors and history of the articles may be found on the source url.
